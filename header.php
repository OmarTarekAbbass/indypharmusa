<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="#">

    <title>Home Indypharmusa</title>

    <link rel='stylesheet' id='astra-theme-css-css'
        href='http://www.mavenexpertsusa.com/wp-content/themes/astra/assets/css/minified/style.min.css?ver=1.8.4'
        type='text/css' media='all' />

    <link rel='stylesheet' href='//fonts.googleapis.com/css?family=Cairo%3A700&#038;display=fallback&#038;ver=1.8.4'
        type='text/css' />
    <link rel='stylesheet' href='css/menu-animation.min.css' type='text/css' />
    <link rel='stylesheet' href='css/style.min.css' type='text/css' />
    <link rel='stylesheet' href='css/style.min2.css' type='text/css' />
    <link rel='stylesheet' href='css/blog-layout-1.min.css' type='text/css' />
    <link rel='stylesheet' href='css/astra-hooks-sticky-header-footer.min.css' type='text/css' />
    <link rel='stylesheet' href='css/style.min3.css' type='text/css' />
    <link rel='stylesheet' href='css/style.min4.css' type='text/css' />
    <link rel='stylesheet' href='css/style.min5.css' type='text/css' />
    <link rel='stylesheet' href='css/style.min6.css' type='text/css' />
    <link rel='stylesheet' href='css/mega-menu.min.css' type='text/css' />
    <link rel='stylesheet' href='css/stylewebsite.css' type='text/css' />
    <link rel='stylesheet' href='css/jet-elements.css' type='text/css' />
    <link rel='stylesheet' href='css/jet-elements-skin.css' type='text/css' />
    <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
        integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/jquery-migrate.min.js'></script>
    <script type='text/javascript' src='js/preloader-script.js'></script>
</head>

<body
    class="postid-703 wp-custom-logo ast-desktop ast-page-builder-template ast-no-sidebar astra-1.8.4 ast-header-custom-item-inside ast-blog-single-style-1 ast-custom-post-type ast-single-post ast-inherit-site-logo-transparent ast-above-mobile-menu-align-stack ast-default-menu-enable ast-default-above-menu-enable ast-default-below-menu-enable ast-advanced-headers ast-full-width-layout ast-inherit-site-logo-sticky elementor-default astra-addon-1.8.1">

    <div id="page" class="hfeed site">
        <header
            class="site-header ast-primary-submenu-animation-fade header-main-layout-1 ast-primary-menu-enabled ast-logo-title-inline ast-hide-custom-menu-mobile ast-menu-toggle-icon ast-mobile-header-inline ast-above-header-mobile-stack ast-below-header-mobile-stack">



            <div class="main-header-bar-wrap">
                <div class="main-header-bar">
                    <div class="ast-container">

                        <div class="ast-flex main-header-container">

                            <div class="site-branding">
                                <div class="ast-site-identity">
                                    <span class="site-logo-img">
                                        <a href="index.php" class="custom-logo-link" rel="home">
                                            <img src="image/logo.png" class="custom-logo" alt="indypharmusa"
                                                sizes="(max-width: 50px) 100vw, 50px" /></a></span>
                                </div>
                            </div>

                            <!-- .site-branding -->
                            <div class="ast-mobile-menu-buttons">


                                <div class="ast-button-wrap">
                                    <button type="button"
                                        class="menu-toggle main-header-menu-toggle  ast-mobile-menu-buttons-minimal "
                                        aria-controls='primary-menu'>
                                        <span class="screen-reader-text">Main Menu</span>
                                        <span class="menu-toggle-icon"></span>
                                    </button>
                                </div>


                            </div>
                            <div class="ast-main-header-bar-alignment">
                                <div class="main-header-bar-navigation">
                                    <nav id="site-navigation" class="ast-flex-grow-1 navigation-accessibility"
                                        role="navigation" aria-label="Site Navigation">
                                        <div class="main-navigation">
                                            <ul id="primary-menu"
                                                class="main-header-menu ast-nav-menu ast-flex ast-justify-content-flex-end  submenu-with-border astra-menu-animation-fade  ast-mega-menu-enabled">
                                                <li id="menu-item-671"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-671">
                                                    <a href="index" class="menu-link "><span
                                                            class="menu-text">Home</span><span
                                                            class="sub-arrow"></span></a>
                                                </li>
                                                <li id="menu-item-672"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-672">
                                                    <a href="about" class="menu-link "><span class="menu-text">About
                                                            us</span><span class="sub-arrow"></span></a>
                                                </li>
                                                <li id="menu-item-673"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-673">
                                                    <a href="#" class="menu-link "><span class="menu-text">Our
                                                            vision</span><span class="sub-arrow"></span></a>
                                                </li>
                                                <li id="menu-item-676"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-676">
                                                    <a href="#" class="menu-link "><span
                                                            class="menu-text">Products</span><span
                                                            class="sub-arrow"></span></a>
                                                </li>
                                                <li id="menu-item-674"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-674">
                                                    <a href="contact" class="menu-link "><span class="menu-text">Contact
                                                            us</span><span class="sub-arrow"></span></a>
                                                </li>
                                                <li class="ast-masthead-custom-menu-items button-custom-menu-item">
                                                    <a class="ast-custom-button-link"
                                                        href="mailto:info@indypharmusa.net"><button
                                                            class=ast-custom-button>info@indypharmusa.net</button></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div><!-- Main Header Container -->
                    </div><!-- ast-row -->
                </div> <!-- Main Header Bar -->
            </div> <!-- Main Header Bar Wrap -->


        </header><!-- #masthead -->

        <div id="content" class="site-content">
            <div class="ast-container">
                <div id="primary" class="content-area primary">
                    <main id="main" class="site-main" role="main">
                        <article id="post-703"
                            class="post-703 pages type-pages status-publish hentry ast-article-single">
                            <div class="ast-post-format- ast-no-thumb single-layout-1 ast-no-date-box">
                                <header class="entry-header ast-header-without-markup">
                                    <div class="ast-single-post-order">
                                    </div>
                                </header><!-- .entry-header -->
                                <div class="entry-content clear" itemprop="text">
                                    <div data-post-id='415' class='insert-page insert-page-415 '>
                                        <div data-elementor-type="post" data-elementor-id="415"
                                            class="elementor elementor-415" data-elementor-settings="[]">
                                            <div class="elementor-inner">
                                                <div class="elementor-section-wrap">