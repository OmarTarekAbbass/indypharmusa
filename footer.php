<section
    class="elementor-element elementor-element-5a32133a elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
    data-id="5a32133a" data-element_type="section"
    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
    <div class="elementor-container elementor-column-gap-no">
        <div class="elementor-row">
            <div class="elementor-element elementor-element-35f91115 elementor-column elementor-col-100 elementor-top-column"
                data-id="35f91115" data-element_type="column">
                <div class="elementor-column-wrap  elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-ea50c17 elementor-widget elementor-widget-heading"
                            data-id="ea50c17" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <span class="elementor-heading-title elementor-size-default">more
                                    information</span>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-8e02dd6 elementor-widget elementor-widget-heading"
                            data-id="8e02dd6" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default">
                                    You need more information
                                    contact us now</h2>
                            </div>
                        </div>
                        <div class="elementor-element elementor-element-91ac968 elementor-align-center envato-kit-111-skew elementor-widget__width-auto elementor-widget elementor-widget-button"
                            data-id="91ac968" data-element_type="widget" data-widget_type="button.default">

                        </div>
                        <div class="elementor-element elementor-element-79865df elementor-align-center envato-kit-111-skew elementor-widget__width-auto elementor-widget elementor-widget-button"
                            data-id="79865df" data-element_type="widget" data-widget_type="button.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-button-wrapper">
                                    <a href="mailto:info@indypharmusa.net"
                                        class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                        <span class="elementor-button-content-wrapper">
                                            <span class="elementor-button-icon elementor-align-icon-left">
                                                <i class="fas fa-envelope-open"></i>
                                            </span>
                                            <span class="elementor-button-text">info@indypharmusa.net</span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>
</div>
</div>
</div>
</div><!-- .entry-content .clear -->
</div>
</article><!-- #post-## -->
</main><!-- #main -->
</div><!-- #primary -->
</div> <!-- ast-container -->
</div><!-- #content -->







<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="ast-small-footer footer-sml-layout-2">
        <div class="ast-footer-overlay">
            <div class="ast-container">
                <div class="ast-small-footer-wrap">
                    <div class="ast-row ast-flex">

                        <div
                            class="ast-small-footer-section ast-small-footer-section-1 ast-small-footer-section-equally ast-col-md-6 ast-col-xs-12">
                            Copyright © 2021 <span class="ast-footer-site-title">Maven Experts Usa Inc.</span>
                        </div>

                        <div
                            class="ast-small-footer-section ast-small-footer-section-2 ast-small-footer-section-equally ast-col-md-6 ast-col-xs-12">
                            Powered by <span class="ast-footer-site-title">Maven Experts Usa Inc.</span> </div>

                    </div> <!-- .ast-row.ast-flex -->
                </div><!-- .ast-small-footer-wrap -->
            </div><!-- .ast-container -->
        </div><!-- .ast-footer-overlay -->
    </div><!-- .ast-small-footer-->


</footer><!-- #colophon -->


</div><!-- #page -->


<a id="ast-scroll-top" class="ast-scroll-top-icon ast-scroll-to-top-right" data-on-devices="both">
    <span class="screen-reader-text">Scroll to Top</span>
</a>
<link rel='stylesheet' href='css/frontend.min.css' type='text/css' />
<link rel='stylesheet' href='css/post-415.css' type='text/css' />
<link rel='stylesheet' href='css/slider-pro.min.css' type='text/css' />
<link rel='stylesheet' href='css/elementor-icons.min.css' type='text/css' />
<link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' />
<link rel='stylesheet' href='css/animations.min.css' type='text/css' />
<link rel='stylesheet' href='css/frontend.min3.css' type='text/css' />
<link rel='stylesheet' href='css/global.css' type='text/css' />
<link rel='stylesheet'
    href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLato%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.2.4'
    type='text/css' />

<script type='text/javascript' src='js/style.min.js'></script>
<script type='text/javascript' src='js/advanced-hooks-sticky-header-footer.min.js'></script>
<script type='text/javascript' src='js/sticky-header.min.js'></script>
<script type='text/javascript' src='js/scroll-to-top.min.js'></script>
<script type='text/javascript' src='js/mega-menu-frontend.min.js'></script>
<script type='text/javascript' src='js/advanced-search.min.js'></script>
<script type='text/javascript' src='js/wp-embed.min.js'></script>
<script type='text/javascript' src='js/imagesloaded.min.js'></script>
<script type='text/javascript' src='js/jquery.sliderPro.min.js'></script>
<script type='text/javascript' src='js/frontend-modules.min.js'></script>
<script type='text/javascript' src='js/jquery.sticky.min.js'></script>
<script type='text/javascript' src='js/frontend.min.js'></script>
<script type='text/javascript' src='js/frontend.min.js'></script>
<script type='text/javascript' src='js/jet-elements.min.js'></script>

<script type='text/javascript' src='js/frontend.js'></script>
<script type='text/javascript' src='js/bootstrap.min.js'></script>
<script>
/(trident|msie)/i.test(navigator.userAgent) && document.getElementById && window.addEventListener && window
    .addEventListener("hashchange", function() {
        var t, e = location.hash.substring(1);
        /^[A-z0-9_-]+$/.test(e) && (t = document.getElementById(e)) && (/^(?:a|select|input|button|textarea)$/i
            .test(t.tagName) || (t.tabIndex = -1), t.focus())
    }, !1);
</script>


<script>
var video = document.getElementById('autoplay')
// When the 'ended' event fires
video.addEventListener('ended', function() {
    // Reset the video to 0
    video.currentTime = 0;
    // And play again
    video.play();
});
</script>
</body>

</html>