<?php include 'header.php'; ?>



<div id="content" class="site-content">
    <div class="ast-container">
        <div id="primary" class="content-area primary">
            <main id="main" class="site-main" role="main">
                <article id="post-703" class="post-703 pages type-pages status-publish hentry ast-article-single">
                    <div class="ast-post-format- ast-no-thumb single-layout-1 ast-no-date-box">
                        <header class="entry-header ast-header-without-markup">
                            <div class="ast-single-post-order">
                            </div>
                        </header><!-- .entry-header -->
                        <div class="entry-content clear" itemprop="text">
                            <div data-post-id='415' class='insert-page insert-page-415 '>
                                <div data-elementor-type="post" data-elementor-id="415" class="elementor elementor-415"
                                    data-elementor-settings="[]">
                                    <div class="elementor-inner">
                                        <div class="elementor-section-wrap">
                                            <section
                                                class="elementor-element elementor-element-5135e2c5 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section">
                                                <video style="width: 100%; height: auto;" playsinline autoplay="true"
                                                    muted id="autoplay">
                                                    <source src="image/INDY_USA_video_intro.mp4" type="video/mp4">
                                                    <source src="image/INDY_USA_video_intro.mp4" type="video/ogg">
                                                </video>
                                            </section>

                                            <section
                                                class="elementor-element elementor-element-5d397f7d elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section"
                                                data-id="5d397f7d" data-element_type="section"
                                                data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-element elementor-element-4242f0f3 elementor-column elementor-col-100 elementor-top-column"
                                                            data-id="4242f0f3" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap  elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <section
                                                                        class="elementor-element elementor-element-5500eec0 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section"
                                                                        data-id="5500eec0" data-element_type="section">
                                                                        <div
                                                                            class="elementor-container elementor-column-gap-default">
                                                                            <div class="elementor-row">
                                                                                <div class="elementor-element elementor-element-1ff8e8c9 elementor-column elementor-col-50 elementor-inner-column"
                                                                                    data-id="1ff8e8c9"
                                                                                    data-element_type="column">
                                                                                    <div
                                                                                        class="elementor-column-wrap  elementor-element-populated">
                                                                                        <div
                                                                                            class="elementor-widget-wrap">
                                                                                            <div class="elementor-element elementor-element-e6f6121 elementor-widget elementor-widget-heading"
                                                                                                data-id="e6f6121"
                                                                                                data-element_type="widget"
                                                                                                data-widget_type="heading.default">
                                                                                                <div
                                                                                                    class="elementor-widget-container">
                                                                                                    <h2
                                                                                                        class="elementor-heading-title elementor-size-default">
                                                                                                        Welcome
                                                                                                        to </h2>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="elementor-element elementor-element-5f2b35e elementor-widget elementor-widget-heading"
                                                                                                data-id="5f2b35e"
                                                                                                data-element_type="widget"
                                                                                                data-widget_type="heading.default">
                                                                                                <div
                                                                                                    class="elementor-widget-container">
                                                                                                    <h2
                                                                                                        class="elementor-heading-title elementor-size-default">
                                                                                                        Indy Pharm

                                                                                                    </h2>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="elementor-element elementor-element-71987bd elementor-widget elementor-widget-divider"
                                                                                                data-id="71987bd"
                                                                                                data-element_type="widget"
                                                                                                data-widget_type="divider.default">
                                                                                                <div
                                                                                                    class="elementor-widget-container">
                                                                                                    <div
                                                                                                        class="elementor-divider">
                                                                                                        <span
                                                                                                            class="elementor-divider-separator"></span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="elementor-element elementor-element-11082591 elementor-widget elementor-widget-text-editor"
                                                                                                data-id="11082591"
                                                                                                data-element_type="widget"
                                                                                                data-widget_type="text-editor.default">
                                                                                                <div
                                                                                                    class="elementor-widget-container">
                                                                                                    <div
                                                                                                        class="elementor-text-editor elementor-clearfix">
                                                                                                        <p>
                                                                                                            We are
                                                                                                            health
                                                                                                            wellness
                                                                                                            enthusiasts.
                                                                                                            How can we
                                                                                                            make skin
                                                                                                            and hair
                                                                                                            care as
                                                                                                            useful,
                                                                                                            simple,
                                                                                                            effective,
                                                                                                            trustworthy
                                                                                                            and fun, for
                                                                                                            as many
                                                                                                            people as
                                                                                                            possible?
                                                                                                            That’s what
                                                                                                            we ask
                                                                                                            ourselves
                                                                                                            each day.
                                                                                                            That’s why
                                                                                                            you’re here
                                                                                                        </p>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="elementor-element elementor-element-37003044 elementor-hidden-tablet elementor-hidden-phone elementor-column elementor-col-50 elementor-inner-column"
                                                                                    data-id="37003044"
                                                                                    data-element_type="column">
                                                                                    <div
                                                                                        class="elementor-column-wrap  elementor-element-populated">
                                                                                        <div
                                                                                            class="elementor-widget-wrap">
                                                                                            <div class="elementor-element elementor-element-5d39f696 elementor-widget elementor-widget-image"
                                                                                                data-id="5d39f696"
                                                                                                data-element_type="widget"
                                                                                                data-widget_type="image.default">
                                                                                                <div
                                                                                                    class="elementor-widget-container">
                                                                                                    <div
                                                                                                        class="elementor-image">
                                                                                                        <img src="image/11.jpg"
                                                                                                            title="landing_bg"
                                                                                                            alt="" />
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>

                                            <section
                                                class="elementor-element elementor-element-7f2df011 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-element elementor-element-18c945c elementor-hidden-tablet elementor-column elementor-col-50 elementor-top-column"
                                                            data-id="18c945c" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap  elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <section
                                                                        class="elementor-element elementor-element-76004c2a elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section elementor-inner-section"
                                                                        data-id="76004c2a" data-element_type="section"
                                                                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                                        <div class="elementor-background-overlay">
                                                                        </div>
                                                                        <div
                                                                            class="elementor-container elementor-column-gap-default">
                                                                            <div class="elementor-row">
                                                                                <div class="elementor-element elementor-element-67a920ad elementor-column elementor-col-100 elementor-inner-column"
                                                                                    data-id="67a920ad"
                                                                                    data-element_type="column">
                                                                                    <div
                                                                                        class="elementor-column-wrap  elementor-element-populated">
                                                                                        <div
                                                                                            class="elementor-widget-wrap">
                                                                                            <div class="elementor-element elementor-element-52ddaa52 elementor-aspect-ratio-11 elementor-widget elementor-widget-video"
                                                                                                data-id="52ddaa52"
                                                                                                data-element_type="widget"
                                                                                                data-settings="{&quot;lightbox&quot;:&quot;yes&quot;,&quot;aspect_ratio&quot;:&quot;11&quot;}"
                                                                                                data-widget_type="video.default">
                                                                                                <div
                                                                                                    class="elementor-widget-container">
                                                                                                    <div
                                                                                                        class="elementor-wrapper elementor-open-lightbox">
                                                                                                        <div
                                                                                                            class="elementor-custom-embed-image-overlay">
                                                                                                            <img src="image/play.png"
                                                                                                                title="play"
                                                                                                                alt=""
                                                                                                                type="button"
                                                                                                                data-toggle="modal"
                                                                                                                data-target="#myModal" />
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </section>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-element elementor-element-58c3e3bd elementor-column elementor-col-50 elementor-top-column"
                                                            data-id="58c3e3bd" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap  elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-10fd3df elementor-widget elementor-widget-spacer"
                                                                        data-id="10fd3df" data-element_type="widget"
                                                                        data-widget_type="spacer.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-spacer">
                                                                                <div class="elementor-spacer-inner">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-9415073 elementor-widget elementor-widget-heading"
                                                                        data-id="9415073" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <span
                                                                                class="elementor-heading-title elementor-size-default">Indy
                                                                                Pharm Philosophy
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-22428f0 elementor-widget elementor-widget-divider"
                                                                        data-id="22428f0" data-element_type="widget"
                                                                        data-widget_type="divider.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-divider">
                                                                                <span
                                                                                    class="elementor-divider-separator"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-34280a8 elementor-widget elementor-widget-heading"
                                                                        data-id="34280a8" data-element_type="widget"
                                                                        data-widget_type="heading.default">

                                                                    </div>
                                                                    <div class="elementor-element elementor-element-83c796c elementor-widget elementor-widget-text-editor"
                                                                        data-id="83c796c" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                Indy Pharm seeks to nurture and
                                                                                celebrate each individual’s unique
                                                                                beauty. Every time you use Indy Pharm
                                                                                USA products, it serves as a reminder
                                                                                that you are beautiful, that you are
                                                                                appreciated and that you are worthy of a
                                                                                nourished life.
                                                                                An elevated natural experience, Indy
                                                                                Pharm takes a holistic approach to
                                                                                beauty that transcends skin and hair
                                                                                care; it is a total experience that
                                                                                works to nurture the mind, body, spirit
                                                                                and emotions. Our extraordinarily unique
                                                                                formulas and exceptional ingredients
                                                                                deliver healthy radiant skin and hair.

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Indypharm usa</h4>
            </div>
            <div class="modal-body">
                <video style="width: 100%; height: auto;" controls>
                    <source src="image/INDY_USA_intro.mp4" type="video/mp4">
                    <source src="image/INDY_USA_intro.mp4" type="video/ogg">
                </video>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>